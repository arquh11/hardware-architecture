![Men Working](https://gitlab.com/iush/hardware-architecture/raw/master/images/menworking.png "Document under maintenance!")

Warnings:
There are some attempts at humor here.

----

# HARDWARE ARCHITECTURE

In engineering, hardware architecture refers to the identification of a system's physical components and their interrelationships.

![HARDWARE ARCHITECTURE]https://en.wikipedia.org/wiki/Hardware_architecture

![Google search](https://gitlab.com/iush/hardware-architecture/raw/master/images/GoogleSearchHardwareArch.png "Google searh: hardware architecture")

----

## Inteligent Classroom before a Inteligent City

We think that we need a clever Classroom before think on a clever City.

----

## A short range, ultra-low power consuming wireless technology.

Shares the “Bluetooth” name, but has different design goals in mind.

----

## Power Consumption

Years, not hours or days.

----

## Short range

About 50m

----

## Packet-based

- BLE is a “star” topology network

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/paket.png "Emparejamiento")

_
====

## The Stack

- Controller
- Host

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack.png "Emparejamiento")

_
====

### Logical Link Control and Adaptation Protocol (L2CAP)

- MTU
- Channel Management
- Connection parameter updates

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack1.png "Emparejamiento")

_
====

### Maximum Transmission Unit (MTU)

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/mtu.png "Emparejamiento")

_
====

### Security Manager Protocol (SMP)

- Pairing
- Authentification
- Bonding

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack2.png "Emparejamiento")

_
====

### Generic Access Profile (GAP)

- Device Discovery
- Link Establishment
- Link Management
- Link Termination
- Initiation of security features

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack3.png "Emparejamiento")

----

### Attribute Protocol (ATT)

- A handle (address)
- A type
- A set of permissions

_
====

### Central vs Peripheral

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/centravsperi.png "Emparejamiento")
centravsperi
_
====

### Generic Attribute Profile (GATT)

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Gatt.png "Emparejamiento")
_
====

### Intervals

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Emparejamiento.png "Emparejamiento")

_
====

### Connection-less
Devices do not need to maintain connections.

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/NetworkStates.png "Emparejamiento")

_
====

### Interval

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/ConInterval.png "Emparejamiento")

_
====

### Pairing

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Link.png "Emparejamiento")

----

## Security
Devices pair, keys are distributed, and the connection is encrypted.

Encryption is AES-128.

----

## Two kinds of services
There are primary services and secondary services.

Services can contain other services. (Nested services)

_
====

## Profiles

Profiles define roles for devices to play.

_
====

## Profiles & Services

Profiles contain services.
Services can be contained by multiple profiles.

_
====

## Anatomy of a Peripheral

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/anatomiPeri.png "Emparejamiento")

----

### Aplication

[Aplication
![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/app.png "Emparejamiento")](https://gitlab.com/oemunoz/force-ha)

_
====

### D3 Library

[D3](https://github.com/d3/d3-force)

_
====

### Bleno Library

[Bleno](https://github.com/sandeepmistry/bleno)

----
Gracias!!
